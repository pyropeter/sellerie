#![deny(unsafe_code)]

use core::convert::TryInto;
use tinyvec::{Array,ArrayVec};

pub struct Queue<A: Array> {
    start: u16,
    data:  ArrayVec<A>,
}

impl<A: Array> Default for Queue<A> {
    fn default() -> Self {
        Self {
            start: A::CAPACITY.try_into().unwrap(),
            data:  ArrayVec::default(),
        }
    }
}

impl<A> Queue<A>
where
    A: Array,
    A::Item: Copy,
{
    pub fn as_slice_mut(&mut self) -> &mut [A::Item] {
        self.start = 0;
        self.data.clear();
        self.data.grab_spare_slice_mut()
    }

    pub fn set_len(&mut self, len: usize) {
        self.data.set_len(len);
    }

    pub fn peek(&self) -> Option<A::Item> {
        let index = usize::from(self.start);
        self.data.as_slice().get(index).map(|x| *x)
    }

    pub fn pop(&mut self) -> Option<A::Item> {
        let res = self.peek();

        if res.is_some() {
            self.start = self.start.checked_add(1).unwrap();
        }

        res
    }

    pub fn is_empty(&self) -> bool {
        self.peek().is_none()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn foo() {
        const TESTSTR: &[u8; 4] = b"test";

        let mut foo: Queue<[u8; 12]> = Queue::default();

        foo.as_slice_mut().split_at_mut(TESTSTR.len()).0.copy_from_slice(TESTSTR);
        foo.set_len(TESTSTR.len());

        assert_eq!(foo.peek(), Some(b't'));
        assert_eq!(foo.pop(),  Some(b't'));
    }
}

