#![deny(unsafe_code)]

use crate::protocol::*;

use tinyvec::{Array,ArrayVec};

enum RxState {
    Buffer,
    Escape,
}

pub struct Rx<A: Array<Item = u8>> {
    message: ArrayVec<A>,
    state: RxState,
}

impl<A: Array<Item = u8>> Default for Rx<A> {
    fn default() -> Self {
        Self {
            message: ArrayVec::default(),
            state: RxState::Buffer,
        }
    }
}

impl<A: Array<Item = u8>> Rx<A> {
    pub fn feed<F, R>(&mut self, byte: u8, parse: F) -> Option<R>
    where
        F: FnOnce(&[u8]) -> R
    {
        match self.state {
            RxState::Buffer => {
                if byte == ESCAPE {
                    self.state = RxState::Escape;
                } else {
                    self.message.try_push(byte);
                }
                None
            },
            RxState::Escape => {
                self.state = RxState::Buffer;
                match byte {
                    ESC_ESC => { self.message.try_push(ESCAPE); None },
                    ESC_SOM => { self.message.clear();          None },
                    ESC_EOM => {
                        let res = parse(self.message.as_slice());
                        self.message.clear();
                        Some(res)
                    },
                    _ => None,
                }
            },
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn rx() {
        let parse = |buf: &[u8]| buf.len();

        let mut rx: Rx<[u8; 16]> = Rx::default();

        assert_eq!(rx.feed(b'a',    parse), None);
        assert_eq!(rx.feed(b'a',    parse), None);
        assert_eq!(rx.feed(b'\xc0', parse), None);
        assert_eq!(rx.feed(b'\x02', parse), Some(2));
    }
}
