#![deny(unsafe_code)]

use crate::protocol::*;
use crate::queue::Queue;
use tinyvec::Array;

#[derive(Debug, PartialEq, Eq)]
enum TxState {
    Normal,
    EscapeSOM,
    EscapeEOM,
    SendESC,
    SendSOM,
    SendEOM,
}

pub struct Tx<A: Array<Item = u8>> {
    message: Queue<A>,
    state:   TxState,
}

impl<A: Array<Item = u8>> Default for Tx<A> {
    fn default() -> Self {
        Self {
            message: Default::default(),
            state:   TxState::Normal,
        }
    }
}

impl<A: Array<Item = u8>> Tx<A> {
    pub fn is_empty(&self) -> bool {
        self.state == TxState::Normal && self.message.is_empty()
    }

    pub fn as_slice_mut(&mut self) -> &mut [u8] {
        if !self.is_empty() {
            panic!("as_slice_mut() called on non-empty Tx");
        }

        self.message.as_slice_mut()
    }

    pub fn submit(&mut self, len: usize) {
        if !self.is_empty() {
            panic!("submit() called on non-empty Tx");
        }

        if len == 0 {
            panic!("submit() called with len == 0");
        }

        self.message.set_len(len);
        self.state = TxState::EscapeSOM;
    }

    pub fn send<F, T, E>(&mut self, mut send: F) -> Result<T, E>
    where
        F: FnMut(u8) -> Result<T, E>
    {
        if self.is_empty() {
            panic!("send() called on empty Tx");
        }

        let byte = match self.state {
            TxState::Normal    => self.message.peek().unwrap(),
            TxState::EscapeSOM => ESCAPE,
            TxState::EscapeEOM => ESCAPE,
            TxState::SendESC   => ESC_ESC,
            TxState::SendSOM   => ESC_SOM,
            TxState::SendEOM   => ESC_EOM,
        };
        let result = send(byte)?;

        let next_state = match self.state {
            TxState::Normal => {
                self.message.pop();

                if byte == ESCAPE {
                    TxState::SendESC
                } else if self.message.is_empty() {
                    TxState::EscapeEOM
                } else {
                    TxState::Normal
                }
            },
            TxState::EscapeSOM => TxState::SendSOM,
            TxState::EscapeEOM => TxState::SendEOM,
            TxState::SendESC => {
                if self.message.is_empty() {
                    TxState::EscapeEOM
                } else {
                    TxState::Normal
                }
            },
            TxState::SendSOM => TxState::Normal,
            TxState::SendEOM => TxState::Normal,
        };

        self.state = next_state;
        Ok(result)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use tinyvec::array_vec;

    #[test]
    fn tx() {
        const TESTPKT: &[u8; 5] = b"te\xC0st";

        let mut tx: Tx<[u8; 16]> = Default::default();

        tx.as_slice_mut().split_at_mut(TESTPKT.len()).0.copy_from_slice(TESTPKT);
        tx.submit(TESTPKT.len());

        let mut dst = array_vec!([u8; 10]);

        while !tx.is_empty() {
            tx.send(|b| -> Result<(), ()> { dst.push(b); Ok(()) }).unwrap();
        }

        assert_eq!(dst.as_slice(), b"\xC0\x01te\xC0\x00st\xC0\x02");
    }
}
