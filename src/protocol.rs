
pub const ESCAPE: u8 = b'\xC0';

pub const ESC_ESC: u8 = b'\x00';
pub const ESC_SOM: u8 = b'\x01';
pub const ESC_EOM: u8 = b'\x02';

