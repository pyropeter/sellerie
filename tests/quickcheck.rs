use quickcheck_macros::quickcheck;
use sellerie::{Rx, Tx};

#[quickcheck]
fn rx_tx(buf: Vec<u8>) -> bool {
    if buf.is_empty() {
        return true;
    }

    let mut tx: Tx<[u8; 128]> = Default::default();
    tx.as_slice_mut().split_at_mut(buf.len()).0.copy_from_slice(&buf);
    tx.submit(buf.len());

    let mut on_wire = Vec::with_capacity(200);
    while !tx.is_empty() {
        tx.send(|b| -> Result<(), ()> { on_wire.push(b); Ok(()) }).unwrap();
    }

    dbg!(&on_wire);

    let mut rx: Rx<[u8; 128]> = Rx::default();

    let (last, prefix) = on_wire.split_last().unwrap();
    for byte in prefix {
        rx.feed(*byte, |_| panic!("x"));
    }

    let decoded = rx.feed(*last, |msg| Vec::from(msg)).unwrap();

    buf == decoded
}

